function findHigher(){
	let num1 = parseFloat(document.getElementById("num1").value);
	let num2 = parseFloat(document.getElementById("num2").value);
	if(num1 > num2){
		document.getElementById("output1").innerHTML = "Highest Number: " + num1;
	}else if(num1 < num2){
			document.getElementById("output1").innerHTML = "Highest Number: " + num2;
	}else if(num1 == num2){
		document.getElementById("output1").innerHTML = "Highest Number: " + "Numbers are equal"
	}else{
		document.getElementById("output1").innerHTML = "Highest Number:"
	}
}

let highernum = document.getElementById("highernum");
highernum.addEventListener("click", findHigher);

function gradeConvert(){
	let grade = parseFloat(document.getElementById("grade").value);
	if(grade <= -1 || grade >= 101){
		document.getElementById("output2").innerHTML = "Grade Conversion: Invalid Grade"
	}else if(grade <= 59){
		document.getElementById("output2").innerHTML = "Grade Conversion: F"
	}else if(grade <= 63){
		document.getElementById("output2").innerHTML = "Grade Conversion: D-"
	}else if(grade <= 66){
		document.getElementById("output2").innerHTML = "Grade Conversion: D"
	}else if(grade <= 69){
		document.getElementById("output2").innerHTML = "Grade Conversion: D+"
	}else if(grade <= 73){
		document.getElementById("output2").innerHTML = "Grade Conversion: C-"
	}else if(grade <= 76){
		document.getElementById("output2").innerHTML = "Grade Conversion: C"
	}else if(grade <= 79){
		document.getElementById("output2").innerHTML = "Grade Conversion: C+"
	}else if(grade <= 83){
		document.getElementById("output2").innerHTML = "Grade Conversion: B-"
	}else if(grade <= 86){
		document.getElementById("output2").innerHTML = "Grade Conversion: B"
	}else if(grade <= 89){
		document.getElementById("output2").innerHTML = "Grade Conversion: B+"
	}else if(grade <= 93){
		document.getElementById("output2").innerHTML = "Grade Conversion: A-"
	}else if(grade <= 100){
		document.getElementById("output2").innerHTML = "Grade Conversion: A"
	}else
		document.getElementById("output2").innerHTML = "Grade Conversion:"
}

let convgrade = document.getElementById("convgrade");
convgrade.addEventListener("click", gradeConvert);

function HelloWorld(){
	let i = document.getElementById("country").selectedIndex;
	let lang = document.getElementById("country").options;
	if(lang[i].index == 1){
		document.getElementById("output3").innerHTML = "Hello World"
	}else if(lang[i].index == 2){
		document.getElementById("output3").innerHTML = "Bonjour Le Monde"
	}else if(lang[i].index == 3){
		document.getElementById("output3").innerHTML = "こんにちは世界"
	}else if(lang[i].index == 4){
		document.getElementById("output3").innerHTML = "Ciao Mondo"
	}else if(lang[i].index == 5){
		document.getElementById("output3").innerHTML = "Hallo Welt"
	}else
		document.getElementById("output3").innerHTML
}

let greeting = document.getElementById("greeting");
greeting.addEventListener("click", HelloWorld);